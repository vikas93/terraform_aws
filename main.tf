provider "aws" {
  region     = "us-east-1"
}

module "vpc" {
  source               = "./modules/tf_vpc"
  name                 = "tf_vpc"
  cidr                 = "172.16.0.0/16"
  enable_dns_hostnames = "False"
  enable_dns_support   = "True"
}
